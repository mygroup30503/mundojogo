package com.mundojogo.info.app.holdem.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mundojogo.info.app.holdem.presentation.screens.info.MundoInfoScreen
import com.mundojogo.info.app.holdem.presentation.screens.loading.MundoLoadingScreen
import com.mundojogo.info.app.holdem.presentation.screens.main.MundoMainScreen
import com.mundojogo.info.app.holdem.presentation.utils.MundoScreenRoutes

@Composable
fun MundoJogoMainNavigation() {
    val navHostController = rememberNavController()
    NavHost(
        navController = navHostController,
        startDestination = MundoScreenRoutes.MUNDO_LOADING_SCREEN
    ) {
        composable(MundoScreenRoutes.MUNDO_MAIN_SCREEN){
            MundoMainScreen(navHostController = navHostController)
        }
        composable(MundoScreenRoutes.MUNDO_LOADING_SCREEN){
            MundoLoadingScreen(navHostController = navHostController)
        }
        composable(MundoScreenRoutes.MUNDO_INFO_SCREEN){
            MundoInfoScreen(navHostController = navHostController)
        }

    }
}