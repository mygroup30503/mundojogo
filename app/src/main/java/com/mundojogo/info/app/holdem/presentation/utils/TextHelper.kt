package com.mundojogo.info.app.holdem.presentation.utils

import android.graphics.Canvas
import android.graphics.Paint
import android.text.style.ReplacementSpan
import androidx.annotation.ColorInt
import androidx.annotation.Dimension

class TextHelper(
    @ColorInt private val sC: Int,
    @Dimension private val sw: Float
): ReplacementSpan() {

    override fun getSize(
        p: Paint,
        t: CharSequence,
        s: Int,
        e: Int,
        fm: Paint.FontMetricsInt?
    ): Int {
        if (fm != null && p.fontMetricsInt != null) {
            fm.bottom = p.fontMetricsInt.bottom
            fm.top = p.fontMetricsInt.top
            fm.descent = p.fontMetricsInt.descent
            fm.leading = p.fontMetricsInt.leading
        }
        return p.measureText(t.toString().substring(s until e)).toInt()
    }


    override fun draw(
        c: Canvas,
        t: CharSequence,
        s: Int,
        e: Int,
        x: Float,
        top: Int,
        y: Int,
        b: Int,
        p: Paint
    ) {
        val originTextColor = p.color

        p.apply {
            color = sC
            style = Paint.Style.STROKE
            this.strokeWidth = this@TextHelper.sw
        }
        c.drawText(t, s, e, x, y.toFloat(), p)

        p.apply {
            color = originTextColor
            style = Paint.Style.FILL
        }
        c.drawText(t, s, e, x, y.toFloat(), p)
    }

}
