package com.mundojogo.info.app.holdem.di

import com.mundojogo.info.app.holdem.data.repository.MundoGetInfoRepImpl
import com.mundojogo.info.app.holdem.data.storage.MundoInfoStorage
import com.mundojogo.info.app.holdem.domain.repository.MundoGetInfoRep
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MundoMainModule {

    @Provides
    @Singleton
    fun provideMundoStorage(): MundoInfoStorage{
        return MundoInfoStorage()
    }

    @Provides
    @Singleton
    fun provideMundoInfoRepository(storage: MundoInfoStorage): MundoGetInfoRep{
        return MundoGetInfoRepImpl(storage)
    }
}