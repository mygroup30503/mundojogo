package com.mundojogo.info.app.holdem.presentation.screens.loading

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavHostController
import com.mundojogo.info.app.holdem.R
import com.mundojogo.info.app.holdem.presentation.navigation.MundoJogoMainNavigation
import com.mundojogo.info.app.holdem.presentation.utils.MundoScreenRoutes
import com.mundojogo.info.app.holdem.presentation.utils.MundoTextStyle
import kotlinx.coroutines.delay

@Composable
fun MundoLoadingScreen(navHostController: NavHostController) {
    LaunchedEffect(key1 = true) {
        delay((1000..3000L).random())
        navHostController.navigate(MundoScreenRoutes.MUNDO_MAIN_SCREEN) {
            popUpTo(MundoScreenRoutes.MUNDO_LOADING_SCREEN) { inclusive = true }
        }
    }
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        MundoTextStyle(
            t = "Loading",
            sw = 14f,
            sc = Color.Black,
            fs = 40f,
            maxLength = 100,
            modifier = Modifier
        )
    }
}