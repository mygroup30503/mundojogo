package com.mundojogo.info.app.holdem.data.repository

import com.mundojogo.info.app.holdem.data.storage.MundoInfoStorage
import com.mundojogo.info.app.holdem.domain.model.MundoInfoModel
import com.mundojogo.info.app.holdem.domain.repository.MundoGetInfoRep

class MundoGetInfoRepImpl(val mundoInfoStorage: MundoInfoStorage): MundoGetInfoRep {
    override fun getNext(): MundoInfoModel? {
        if (mundoInfoStorage.mundoCounter>=mundoInfoStorage.mundoList.size-1) return null
        mundoInfoStorage.mundoCounter++
        return mundoInfoStorage.mundoList[mundoInfoStorage.mundoCounter]
    }

    override fun getLast(): MundoInfoModel? {
        if (mundoInfoStorage.mundoCounter==0) return null
        mundoInfoStorage.mundoCounter--
        return mundoInfoStorage.mundoList[mundoInfoStorage.mundoCounter]
    }
}