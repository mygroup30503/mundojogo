package com.mundojogo.info.app.holdem.presentation.utils

import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.viewinterop.AndroidView

@Composable
fun MundoTextStyle(t: String, sw: Float, sc: Color, fs: Float, maxLength: Int, modifier: Modifier) {

    fun String.sts(maxLength: Int): List<String> {
        val list = mutableListOf<String>()
        var currentString = ""
        var currentWord = ""

        fun addCurrentString() {
            if (currentString.isNotEmpty()) {
                list.add(currentString)
                currentString = ""
            }
        }

        this.forEach { char ->
            if (char.isWhitespace()) {
                if (currentString.length + currentWord.length + 1 <= maxLength) {
                    // Если добавление текущего слова не приведет к превышению maxLength
                    if (currentString.isNotEmpty()) {
                        currentString += " "
                    }
                    currentString += currentWord
                    currentWord = ""
                } else {
                    // Если добавление текущего слова приведет к превышению maxLength
                    addCurrentString()
                    currentString = currentWord
                    currentWord = ""
                }
            } else {
                currentWord += char
            }
        }

        // Добавляем оставшиеся символы


        // Проверяем, если оставшиеся слова не умещаются в maxLength
        if (currentWord.isNotEmpty()) {
            // Проверяем, если текущая строка не пуста
            if (currentString.isNotEmpty()) {
                // Пробуем объединить последнее слово и текущую строку
                val combinedString = "$currentString $currentWord"
                if (combinedString.length <= maxLength) {

                    // Если объединение умещается, добавляем объединенную строку
                    list.add(combinedString)
                } else {
                    // Если объединение не умещается, добавляем отдельно
                    list.add(currentString)
                    list.add(currentWord)
                }
            } else {
                // Если текущая строка пуста, добавляем отдельно
                list.add(currentWord)
            }
        }

        return list
    }



    Column(modifier = modifier) {
        t.sts(maxLength).forEach{
            val spannable = SpannableString(it)
            val os = TextHelper(sc.toArgb(), sw)
            spannable.setSpan(os, 0, it.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            MyText(size = fs, sp = spannable)
        }
    }
}

@Composable
fun MyText(size: Float, sp: SpannableString) {
    Log.d("tag", sp.toString())

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()

    ) {
        AndroidView(
            factory = { context ->
                TextView(context).apply {
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    textSize = size
                    setTextColor(Color.White.toArgb())
                    gravity = Gravity.CENTER_HORIZONTAL
                    setText(sp, TextView.BufferType.SPANNABLE)
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(Color.Transparent),
            update = {view->
                view.text = sp
            }

        )
    }
}