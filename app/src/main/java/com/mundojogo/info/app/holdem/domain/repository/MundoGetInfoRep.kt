package com.mundojogo.info.app.holdem.domain.repository

import com.mundojogo.info.app.holdem.domain.model.MundoInfoModel

interface MundoGetInfoRep {
    fun getNext(): MundoInfoModel?
    fun getLast(): MundoInfoModel?
}