package com.mundojogo.info.app.holdem

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MundoJogoApp: Application()