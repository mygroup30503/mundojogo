package com.mundojogo.info.app.holdem.presentation.utils

object MundoScreenRoutes {
    const val MUNDO_MAIN_SCREEN = "wrbwcrr,iwgnpwr[of[pwcgm"
    const val MUNDO_INFO_SCREEN = "gwrgtttergtraegrwg"
    const val MUNDO_LOADING_SCREEN = "frefoiqrp[fjp[prie[p"
}