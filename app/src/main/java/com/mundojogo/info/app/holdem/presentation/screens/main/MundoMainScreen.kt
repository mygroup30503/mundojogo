package com.mundojogo.info.app.holdem.presentation.screens.main

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.mundojogo.info.app.holdem.R
import com.mundojogo.info.app.holdem.presentation.utils.MundoScreenRoutes

@Composable
fun MundoMainScreen(navHostController: NavHostController) {
    val activity = (LocalContext.current as? Activity)
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(1f)) {

            }
            Column(modifier = Modifier.weight(3f)) {
                Image(painter = painterResource(id = R.drawable.btn_start),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            navHostController.navigate(MundoScreenRoutes.MUNDO_INFO_SCREEN)
                        }, contentScale = ContentScale.FillWidth)
                Spacer(modifier = Modifier.height(16.dp))
                Image(painter = painterResource(id = R.drawable.btn_exit),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                        activity?.finish()
                    }, contentScale = ContentScale.FillWidth)
            }
            Column(modifier = Modifier.weight(1f)) {

            }
        }
    }
}