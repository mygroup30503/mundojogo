package com.mundojogo.info.app.holdem.domain.usecases

import com.mundojogo.info.app.holdem.domain.model.MundoInfoModel
import com.mundojogo.info.app.holdem.domain.repository.MundoGetInfoRep
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class GetInfoUseCase @Inject constructor(
    private val repository: MundoGetInfoRep
) {
    val data = MutableStateFlow(repository.getNext()!!)
    suspend fun getNext(){
        repository.getNext()?.let { data.emit(it) }
    }

    suspend fun getLast(){
        repository.getLast()?.let { data.emit(it) }
    }
}