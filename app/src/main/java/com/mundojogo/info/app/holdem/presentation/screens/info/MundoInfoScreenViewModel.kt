package com.mundojogo.info.app.holdem.presentation.screens.info

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mundojogo.info.app.holdem.domain.model.MundoInfoModel
import com.mundojogo.info.app.holdem.domain.usecases.GetInfoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MundoInfoScreenViewModel @Inject constructor(
    val useCase: GetInfoUseCase
): ViewModel() {
    val n = useCase.data

    fun getNext(){
        viewModelScope.launch {
            useCase.getNext()
        }

    }

    fun getLast(){
        viewModelScope.launch {
            useCase.getLast()
        }
    }

}