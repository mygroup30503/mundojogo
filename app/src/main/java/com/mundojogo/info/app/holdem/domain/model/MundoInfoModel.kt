package com.mundojogo.info.app.holdem.domain.model

data class MundoInfoModel(
    val title: String,
    val list: Array<Any?>
)
