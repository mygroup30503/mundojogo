package com.mundojogo.info.app.holdem.presentation.screens.info

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.mundojogo.info.app.holdem.R
import com.mundojogo.info.app.holdem.domain.model.MundoInfoModel
import com.mundojogo.info.app.holdem.presentation.utils.MundoTextStyle

@Composable
fun MundoInfoScreen(navHostController: NavHostController) {
    val vm: MundoInfoScreenViewModel = hiltViewModel()
    val data = vm.n.collectAsState()
    InfoPart(data = data.value, navHostController = navHostController, {
        vm.getNext()
    }, {
        vm.getLast()
    })


}

@Composable
fun InfoPart(data: MundoInfoModel, navHostController: NavHostController, onNext: () -> Unit, onLast: () -> Unit) {
    val scrolling = rememberScrollState()
    Column(modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            Box(modifier = Modifier.align(Alignment.Center)) {
                Image(
                    painter = painterResource(id = R.drawable.title_bg),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxHeight()
                        .align(Alignment.Center)
                        .offset(y = 4.dp),
                    contentScale = ContentScale.FillHeight
                )
                MundoTextStyle(
                    t = data.title,
                    sw = 8f,
                    sc = Color.Black,
                    fs = 12f,
                    15,
                    modifier = Modifier.align(Alignment.Center)
                )
            }
            Image(painter = painterResource(id = R.drawable.btn_back), contentDescription = null,
                modifier = Modifier
                    .clickable {
                        onLast.invoke()
                    }
                    .align(Alignment.CenterStart)
                    .padding(8.dp)
            )
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(8f)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(scrolling)

            ) {
                data.list.forEach {
                    when (it) {
                        is String -> {
                            MundoTextStyle(
                                t = it,
                                sw = 10f,
                                sc = Color.Black,
                                fs = 16f,
                                46,
                                modifier = Modifier
                                    .fillMaxSize()
                            )
                        }

                        is Int -> {
                            Image(
                                painter = painterResource(id = it),
                                contentDescription = null,
                                modifier = Modifier.fillMaxWidth(),
                                contentScale = ContentScale.FillWidth
                            )
                        }

                        else -> {

                        }
                    }
                }
            }
        }
        Box(
            modifier = Modifier
                .padding(vertical = 4.dp)
                .fillMaxWidth()
                .weight(1f),
            contentAlignment = Alignment.Center
        ) {
            if (data.title != "Cash Games vs Tournaments") Image(
                painter = painterResource(id = R.drawable.btn_next),
                contentDescription = null,
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .fillMaxHeight()
                    .clickable {
                    onNext.invoke()
                },
                contentScale = ContentScale.FillHeight
            ) else Image(
                painter = painterResource(id = R.drawable.btn_to_menu),
                contentDescription = null,
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .fillMaxHeight()
                    .clickable {
                    navHostController.popBackStack()
                },
                contentScale = ContentScale.FillHeight
            )
        }
    }
}

